package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        TemperatureConversion quoter = new TemperatureConversion();
        double price = quoter.getTemperature("1");
        Assertions.assertEquals(33.8, price);
    }
}