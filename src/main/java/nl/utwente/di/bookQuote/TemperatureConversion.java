package nl.utwente.di.bookQuote;



public class TemperatureConversion {

    
    public double getTemperature(String temperature) {
        int temp = Integer.parseInt(temperature);
        return (temp * 1.8) + 32;
    }

}
